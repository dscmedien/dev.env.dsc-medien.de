"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

//
// Name: PrivacyHandler.js
// Version: 1.0.0
//
var PrivacyHandler =
/*#__PURE__*/
function () {
  function PrivacyHandler(classNames, textContent, settings) {
    _classCallCheck(this, PrivacyHandler);

    this.classNames = classNames;
    this.textContent = textContent;
    this.settings = settings;
    this.state = "undefined";
    this.privacyHandlerCookie = this.getCookie("privacyHandlerCookie");

    if (this.hasAccepted()) {
      this.state = "accepted";
      return null;
    } else {
      this.build();
    }
  }

  _createClass(PrivacyHandler, [{
    key: "build",
    value: function build() {
      this.container = this.generateContainer();
      this.containerInner = this.generateContainerInner();
      this.message = this.generateMessage();
      this.readMore = this.generateReadMore();
      this.AcceptButton = this.generateAcceptButton();
    }
  }, {
    key: "generateContainer",
    value: function generateContainer() {
      var container = this.generateElement("div", this.classNames["container"]);
      document.body.insertBefore(container, document.body.firstChild);
      return container;
    }
  }, {
    key: "generateContainerInner",
    value: function generateContainerInner() {
      var containerInner = this.generateElement("div", this.classNames["container-inner"]);
      this.container.appendChild(containerInner);
      return containerInner;
    }
  }, {
    key: "generateMessage",
    value: function generateMessage() {
      var message = this.generateElement("span", this.classNames["message"]);
      var messageText = document.createTextNode(this.textContent["message"]);
      message.appendChild(messageText);
      this.containerInner.appendChild(message);
      return message;
    }
  }, {
    key: "generateReadMore",
    value: function generateReadMore() {
      var readMore = this.generateElement("a", this.classNames["readMore"]);
      readMore.href = this.settings["readMoreLink"];
      readMore.target = "_blank";
      this.message.appendChild(readMore);
      var readMoreText = document.createTextNode(this.textContent["readMore"]);
      readMore.appendChild(readMoreText);
      return readMore;
    }
  }, {
    key: "generateAcceptButton",
    value: function generateAcceptButton() {
      var AcceptButtonContainer = this.generateElement("div", this.classNames["acceptButtonContainer"]);
      this.containerInner.appendChild(AcceptButtonContainer);
      var AcceptButton = this.generateElement("button", this.classNames["acceptButton"]);
      AcceptButton.addEventListener('click', this.handleAccept.bind(this));
      AcceptButtonContainer.appendChild(AcceptButton);
      var AcceptButtonText = document.createTextNode(this.textContent["acceptButton"]);
      AcceptButton.appendChild(AcceptButtonText);
      return AcceptButton;
    }
  }, {
    key: "generateElement",
    value: function generateElement(tagName, classNames) {
      var element = document.createElement(tagName);
      element.className = classNames;
      return element;
    }
  }, {
    key: "handleAccept",
    value: function handleAccept() {
      this.setCookie("privacyHandlerCookie", "accepted", 99999);
      window.location.reload();
    }
  }, {
    key: "hasAccepted",
    value: function hasAccepted() {
      return this.privacyHandlerCookie == "accepted";
    }
  }, {
    key: "getCookie",
    value: function getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');

      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];

        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }

        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }

      return "";
    }
  }, {
    key: "setCookie",
    value: function setCookie(cname, cvalue, exdays) {
      var d = new Date();
      d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
      var expires = "expires=" + d.toUTCString();
      document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
  }]);

  return PrivacyHandler;
}();

var classNames = {
  "container": "privacy-handler bg-medium-grey",
  "container-inner": "grid-1-sm grid-2-md grid-lmt p-3",
  "message": "message",
  "readMore": "pl-2 read-more",
  "acceptButton": "btn accept",
  "acceptButtonContainer": "mt-3 mt-md-0"
};
var textContent = {
  "message": "Unsere Webseite nutzt Cookies für Funktions- und Statistikzwecke. Wenn Sie der Verwendung von Cookies zustimmen, klicken Sie bitte auf „Einverstanden“.",
  "readMore": "Weitere Informationen",
  "acceptButton": "Einverstanden"
};
var settings = {
  "readMoreLink": "/datenschutzerklaerung/"
};
var myPrivacyHandler = new PrivacyHandler(classNames, textContent, settings);
"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

//
// Name: Topbar.js
// Version: 1.0.0
//
var Topbar =
/*#__PURE__*/
function () {
  function Topbar(id, menuContainerId, mobileToggleId) {
    _classCallCheck(this, Topbar);

    this.topbar = document.getElementById(id);
    this.mobileMenu = document.getElementById(menuContainerId);
    this.burgerButton = document.getElementById(mobileToggleId);
    this.burgerButton.addEventListener('click', this.handleToggle.bind(this));
  }

  _createClass(Topbar, [{
    key: "handleToggle",
    value: function handleToggle() {
      this.topbar.classList.toggle("menu-active");
      this.mobileMenu.classList.toggle("active");
      this.burgerButton.classList.toggle("active");
    }
  }]);

  return Topbar;
}();

var myTopbar = new Topbar("topbar", "nuumenu", "burgerbutton");
