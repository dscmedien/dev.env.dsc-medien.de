<?php
/**
 * Enhance the theme head by hooking into WordPress
 *
 * @package WordPress
 * @subpackage wpnuuli
 * @since 1.0.0
 */

/**
 * adds misc informationen to wordpress head
 */
function nuu_head() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'nuu_head' );

/**
 * adds misc informationen to wordpress head
 */
function nuu_head_clean() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  	remove_action( 'wp_print_styles', 'print_emoji_styles' );
}
add_action( 'after_setup_theme', 'nuu_head_clean' );
