//
// Name: PrivacyHandler.js
// Version: 1.0.0
//

class PrivacyHandler {

    constructor(classNames, textContent, settings) {

        this.classNames = classNames;
        this.textContent = textContent;
        this.settings = settings;

        this.state = "undefined";
        this.privacyHandlerCookie = this.getCookie("privacyHandlerCookie");

        if (this.hasAccepted()) {
            this.state = "accepted";
            return null;
        } else {
            this.build();
        }
    }

    build() {
        this.container = this.generateContainer();
        this.containerInner = this.generateContainerInner();
        this.message = this.generateMessage();
        this.readMore = this.generateReadMore();
        this.AcceptButton = this.generateAcceptButton();
    }

    generateContainer() {
        let container = this.generateElement("div", this.classNames["container"]);
        document.body.insertBefore(container, document.body.firstChild);

        return container;
    }

    generateContainerInner() {
        let containerInner = this.generateElement("div", this.classNames["container-inner"]);
        this.container.appendChild(containerInner);

        return containerInner;
    }

    generateMessage() {
        let message = this.generateElement("span", this.classNames["message"]);
        let messageText = document.createTextNode(this.textContent["message"]);

        message.appendChild(messageText);
        this.containerInner.appendChild(message);

        return message;
    }

    generateReadMore() {
        let readMore = this.generateElement("a", this.classNames["readMore"]);
        readMore.href = this.settings["readMoreLink"];
        readMore.target = "_blank";
        this.message.appendChild(readMore);

        var readMoreText = document.createTextNode(this.textContent["readMore"]);
        readMore.appendChild(readMoreText);

        return readMore;
    }

    generateAcceptButton() {
        let AcceptButtonContainer = this.generateElement("div", this.classNames["acceptButtonContainer"]);
        this.containerInner.appendChild(AcceptButtonContainer);

        let AcceptButton = this.generateElement("button", this.classNames["acceptButton"]);
        AcceptButton.addEventListener('click', this.handleAccept.bind(this));
        AcceptButtonContainer.appendChild(AcceptButton);

        let AcceptButtonText = document.createTextNode(this.textContent["acceptButton"]);
        AcceptButton.appendChild(AcceptButtonText);

        return AcceptButton;
    }

    generateElement(tagName, classNames) {
        let element = document.createElement(tagName);
        element.className = classNames;

        return element;
    }

    handleAccept() {
        this.setCookie("privacyHandlerCookie", "accepted", 99999);
        window.location.reload();
    }

    hasAccepted() {
        return this.privacyHandlerCookie == "accepted";
    }

    getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
}



let classNames = {
    "container": "privacy-handler bg-medium-grey",
    "container-inner": "grid-1-sm grid-2-md grid-lmt p-3",
    "message": "message",
    "readMore": "pl-2 read-more",
    "acceptButton": "btn accept",
    "acceptButtonContainer": "mt-3 mt-md-0"
}

let textContent = {
    "message": "Unsere Webseite nutzt Cookies für Funktions- und Statistikzwecke. Wenn Sie der Verwendung von Cookies zustimmen, klicken Sie bitte auf „Einverstanden“.",
    "readMore": "Weitere Informationen",
    "acceptButton": "Einverstanden"
}

let settings = {
    "readMoreLink": "/datenschutzerklaerung/"
}

let myPrivacyHandler = new PrivacyHandler(classNames, textContent, settings);