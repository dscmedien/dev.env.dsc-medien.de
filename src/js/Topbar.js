//
// Name: Topbar.js
// Version: 1.0.0
//

class Topbar {

    constructor(id, menuContainerId, mobileToggleId) {

        this.topbar = document.getElementById(id);
        this.mobileMenu = document.getElementById(menuContainerId);
        this.burgerButton = document.getElementById(mobileToggleId);

        this.burgerButton.addEventListener('click', this.handleToggle.bind(this));
    }

    handleToggle() {
        this.topbar.classList.toggle("menu-active");
        this.mobileMenu.classList.toggle("active");
        this.burgerButton.classList.toggle("active");
    }

}

let myTopbar = new Topbar("topbar", "nuumenu", "burgerbutton");