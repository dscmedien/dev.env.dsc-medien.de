<?php
/**
 * dsc custom hero
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage wpnuuli
 * @since 1.0.0
 */
?>

<?php
	$services = [
		['src' => '/dist/media/phone--ansprechendes-webdesign.png', 'title' => '#', 'link' => '#'],
		['src' => '/dist/media/phone--erfolgreich-online-verkaufen.png', 'title' => '#', 'link' => '#'],
		['src' => '/dist/media/phone--fachkraefte-finden.png', 'title' => '#', 'link' => '#'],
		['src' => '/dist/media/phone--automatisierte-kundengewinnung.png', 'title' => '#', 'link' => '#'],
	]
?>
 
<header id="hero" class="bg-light-grey">

		<section class="head-content">

			<div class="grid-2-sm grid-lmt grid-jst-end px-3">

				<h1 class="slogan mt-9 px-4 py-6 aligncenter">
					<span class="fw-bold">mehr Kunden <br /> mehr Umsatz <br /> mehr Freiraum</span>
					<span class="amp"> &amp; </span>
					<span class="subline clr-black mb-0 p-2">Automatisierung bringt Ihnen die Zeit zurück</span><br /><span class="subline last clr-black mb-0 p-2">die Sie für das Wesentliche brauchen.</span>
				</h1>

				<div class="cta">
						<a class="btn" href="">kaufen kaufen kaufen</a>
				</div>

			</div>

		</section>


	<section class="phone-slide aligncenter">

	<?php foreach ($services as $item) { ?>

		<a href="<?php echo $item['link'] ?>" title="<?php echo $item['title'] ?>">
			<img class="phone px-4" src="<?php echo get_template_directory_uri()?><?php echo $item['src'] ?>">
		</a>

	<?php } ?>

	</section>

</header><!-- #masthead -->
