<?php
/**
 * topbar menu
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage wpnuuli
 * @since 1.0.0
 */
?>

<div id="nuumenu" class="menu-container py-6 px-4 bg-white">

<?php 
    if ( has_nav_menu( 'primary' ) ) : 
                
        wp_nav_menu(
            array(
                'theme_location' => 'primary',
                'container'      => false,
                'menu_class'     => 'menu animated-border clr-black fw-bold',
                'items_wrap'     => '<ul id="%1$s" class="%2$s" tabindex="0">%3$s</ul>',
            )
        );
                
    endif; 

    if ( has_nav_menu( 'secondary' ) ) : 
                
        wp_nav_menu(
            array(
                'theme_location' => 'secondary',
                'container'      => false,
                'menu_class'     => 'menu fs-small ml-md-4 clr-black',
                'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            )
        );
                
    endif; 

    if ( has_nav_menu( 'icon' ) ) : 
                
        wp_nav_menu(
            array(
                'theme_location' => 'icon',
                'container'      => false,
                'menu_class'     => 'menu icon ml-md-4 clr-black',
                'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            )
        );
                
    endif; 
?>
</div>