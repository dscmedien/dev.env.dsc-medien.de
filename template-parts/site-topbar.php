<?php
/**
 * the topbar
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage wpnuuli
 * @since 1.0.0
 */
?>
 

<nav id="topbar" class="bg-white">
	<div class="grid-2-sm grid-lmt grid-jst-end px-3">

	<?php 
		if ( has_custom_logo() ) :

			$branding_id = get_theme_mod( 'custom_logo' );
			$branding_url = wp_get_attachment_image_url( $branding_id , 'full' );
			echo '<a class="branding py-2" href="' . get_home_url() . '"><img src="' . esc_url( $branding_url ) . '" alt="' . get_bloginfo( 'name' ) . '"></a>';

		endif;

	?>

<button aria-label="Menü" id="burgerbutton" class="burger--button animated-arrow">
	<div class="stripes">
		<div class="top"></div>
		<div class="mid"></div>
		<div class="bot"></div>
	</div>
	<span>Menü</span>
</button>

	</div>
</nav><!-- .layout-wrap -->
