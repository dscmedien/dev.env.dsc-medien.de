<?php
/**
 * the masthead
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage wpnuuli
 * @since 1.0.0
 */
?>
 

<header id="masthead">

	<figure class="<?php echo is_front_page() ? 'featured-image front-page' : 'featured-image'; ?>">
		
		<?php the_post_thumbnail(); ?>

		<?php if( is_front_page()) : ?>

			<figcaption class="mx-3 mb-4 mx-lg-6">
				<span class="h1 clr-black"><?php echo get_bloginfo( 'name' ); ?></span><br>
				<span class="h3 fw-normal clr-black"><?php echo get_bloginfo( 'description' ); ?></span>
			</figcaption>

		<?php endif; ?>

	</figure><!-- .post-thumbnail -->

</header><!-- #masthead -->
