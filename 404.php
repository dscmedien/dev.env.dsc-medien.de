<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage wpnuuli
 * @since 1.0.0
 */

get_header();
?>

	<section id="primary" class="error-404">

		<header class="page-header">
			<h1 class="page-title"><?php _e( 'Entschuldigung! Diese Seite kann nicht gefunden werden.', 'wpnuuli' ); ?></h1>
		</header>

	</section><!-- #primary -->

<?php
get_footer();
