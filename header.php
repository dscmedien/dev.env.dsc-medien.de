<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage wpnuuli
 * @since 1.0.0
 */
?>
 
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="grid-1-sm">

	<?php get_template_part( 'template-parts/site', 'topbar' ); ?>

	<?php get_template_part( 'template-parts/site', 'menu' ); ?>


	<?php if(is_home()) :

		get_template_part( 'template-parts/site', 'masthead' );

	else:
	
		get_template_part( 'template-parts/site', 'hero' );

	endif;
		
	?>

	<main id="content" class="grid-1-sm py-6">
