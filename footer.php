<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage wpnuuli
 * @since 1.0.0
 * 
 */

?>

</main><!-- #content -->

	<footer id="footer" class="clr-white bg-dark-primary">
		<div class="grid-1-sm grid-2-md grid-lmt py-6 px-3">

			<?php 
				if ( is_active_sidebar( 'sidebar-1' ) ) {
					dynamic_sidebar( 'sidebar-1' );
				}
			?>
		
		</div><!-- .grid -->
	</footer><!-- #footer -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
